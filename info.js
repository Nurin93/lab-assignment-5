function Info(){
	//a) Retrieve the inputs.
	var name = document.forms["myForm"]["name"].value;
	var length = document.forms["myForm"]["length"].value;
	var width = document.forms["myForm"]["width"].value;
	var height = document.forms["myForm"]["height"].value;
	var mode = document.forms["myForm"]["mode"].value;
	var type = document.forms["myForm"]["type"].value;

	//b) Calculate the weight using this formula
	var weight = (length * width * height) / 5000;

	//display total cost
	alert("Parcel Volumetric and Cost Calculator"+"\nCustomer name: "+ name+"\nLength: "+ length + " cm"+"\nWidth: "+ 
		width + " cm"+"\nHeight: "+ height + " cm"+"\nWeight: "+ weight + " kg"+"\nMode: "+ mode+"\nType: "+ 
		type+"\nDelivery cost: RM "+calcCost(mode,type,weight));
}

// c(i) display an alert box when reset button is clicked
function resetMessage(){
	alert("The input will be reset.");
}

// c(ii) Transform the input text for customer name to upper case when you leave the input field.
function nameCapitalized(){
        var name = document.getElementById("name");
        name.value=name.value.toUpperCase();
}

// c(iii) Calculate the cost for the parcel based on the mode
function calcCost(mode,type,weight){
	var cost;

	//calculate cost when type:domestic
	if (type == "Domestic"){
	    if (weight < 2){
            if (mode == "Surface"){
                cost = 7;
            }
            else if (mode == "Air"){
                cost = 10;
            }
        }
        
        else if (weight >= 2){
            if (mode == "Surface"){
                cost = 7 + (1.50 * (weight-2));
            }
            else if (mode=="Air"){
                cost = 10 + (3 * (weight-2));
            }
        }     
	} 
	//calculate cost when type:international
	else if (type == "International"){
	    if (weight < 2){
            if (mode == "Surface"){
                cost = 20;
            }
            else if (mode=="Air"){
                cost = 50;
            }
        }

        else if (weight >= 2){
            if (mode == "Surface"){
                cost = 20 + (3 * (weight-2));
            }
            else if (mode == "Air"){
                cost = 50 + (5 * (weight-2));
            }
        }      
	}
	return cost;
}

